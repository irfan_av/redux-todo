
import { ActionTypes } from '../constant/constant';

const INITIAL_STATE = {
    userName: 'Irfan',
    list: [
        {
            name: 'Irfan',
            complete: false
        },
        {
            name: 'Khan',
            complete: false
        },
        {
            name: 'Hello',
            complete: false
        }
    ]
}

function reducer(state = INITIAL_STATE, action) {
    switch(action.type) {
        case ActionTypes.CHANGENAME:
            return({
                ...state,
                userName: action.paylaod
            })
        case ActionTypes.DELETEVALUE:
            return({
                ...state,
                list: action.paylaod
            })
        default:
            return state
    }
}

export default reducer;