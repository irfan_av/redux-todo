
import allReducer from './reducers/index'
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';


const store = createStore(
    allReducer,
    {},
    applyMiddleware(thunk)
)

export default store;