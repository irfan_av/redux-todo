import React, { Component } from 'react';
import './App.css';
import TodoList from './components/TodoList';
import TodoForm from './components/TodoForm';
import { connect } from 'react-redux';
import { changeState, deleteItem } from './store/action/action';

class App extends Component {
  constructor() {
    super();

    this.state = {
      val: '',
      text: '',
      tasks: [
        {
          name: 'Irfan',
          complete: false
        },
        {
          name: 'Khan',
          complete: false
        },
        {
          name: 'Hello',
          complete: false
        }
      ],
      isEditing: false
    }

    this.changeHandler = this.changeHandler.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
    this.del = this.del.bind(this);
    this.editHandler = this.editHandler.bind(this)


  }
  editHandler(index, newValue) {
    console.log(index, newValue);
    let tasks = this.state.tasks;
    let task = tasks[index];
    console.log(task)
    task['name'] = newValue;

    this.setState({
      tasks: tasks
    })
  }


  changeHandler(event) {
    this.setState({
      text:event.target.value
    })
    
  }

  submitHandler(event) {

    event.preventDefault();
    
    let tasks = this.state.tasks;
    let text = this.state.text;
    tasks.push({name: text, complete: false});
    this.setState({
      tasks: tasks,
      text: ''
    })

  }

  del(index) {
  
    let tasks = this.state.tasks;
    tasks.splice(index, 1);
    this.setState({
      tasks: tasks
    })

    console.log('Delete')
  }

  changeValue(event) {
    this.setState({
      val: event.target.value
    })
  }

  clicked() {
    this.props.changeNameByDispatch(this.state.val);
  }

  deleteHandler(index) {
    this.props.deleteDispatch(this.props.list, index);
    console.log(index)
  }

  render() {

    return (
      <div className="App">

        <h1>Todo</h1>

        <TodoForm text={this.state.text}
         changeHandler={this.changeHandler}
         submitHandler={this.submitHandler} />

        <hr/>
        {
          this.state.tasks.map((task, index) => {
            return (
              <TodoList key={index}
               task={task} 
               index={index}
               del={this.del}
               editHandler={this.editHandler} />
            )
           
          })
        }
        <br /><br/><br/>
        <input type='text' value={this.state.val} onChange={this.changeValue.bind(this)}/>
        <button onClick={this.clicked.bind(this)}>change</button>
        <div>
          <ul>
            {
              this.props.list.map((item, index) => {
                return(
                  <li key={index}>{item.name}
                      <button onClick={this.deleteHandler.bind(this, index)}>Delete</button>
                  </li>
                )
               
              })

            }
        
          </ul>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return({
    name: state.rootReducer.userName,
    list: state.rootReducer.list
  })
}
function mapDispatchToProps(dispatch) {
  return({
    changeNameByDispatch: (val) => {
      dispatch(changeState(val));
    },
    deleteDispatch: (list, index) => {
      dispatch(deleteItem(list, index));
    }
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
