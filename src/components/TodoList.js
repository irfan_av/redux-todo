
import React, { Component } from 'react';



class TodoList extends Component {

    constructor(prop) {
        super(prop);
        this.state = {
            editText: '',
            isEditing: false
        }
       
        this.edit = this.edit.bind(this);
        this.submitHandle = this.submitHandle.bind(this);
    }

   
    renderForm() {
        return(
            <div>
                <form onSubmit={this.submitHandle}>
                    <input type="text" defaultValue={this.props.task.name} ref={(value) => {
                        this.input = value;
                    }} />
                    <button type="submit">Update</button>
                </form>
            </div>
        )
    }
    edit(index) {
        let { isEditing } = this.state.isEditing;
        this.setState({
          isEditing: !isEditing
        })
      }

      submitHandle(event) {
        event.preventDefault();
        let val = this.input.value;
        console.log(val)
        this.props.editHandler(this.props.index, this.input.value);
       
        this.setState({
          isEditing: false
        })

    }


    renderList() {
        return (
            <li>
            {this.props.task.name}
            <button onClick={(event, index) => {
                event.stopPropagation();
                this.props.del(this.props.index);
            }}>Delete</button>

            <button onClick={(event, index) => {
                event.stopPropagation();
                this.edit(this.props.index)
            }}>
                Edit
            </button>
        </li>
        )
    }

    
    render() {
        const { isEditing } = this.state;
        return (

            <div>
               {
                    isEditing? this.renderForm() : this.renderList()
               }
            </div>
        
        )
    }
}

export default TodoList;