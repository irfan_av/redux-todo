

import React, { Component } from 'react';

class TodoForm extends Component {
        
    
    render() {
        return (
            <div>
                <form onSubmit={this.props.submitHandler}>
                    <input type="text" value={this.props.text} 
                    onChange={this.props.changeHandler}/>
                    <button type="submit">Submit</button>
                </form>
            </div>
        )
    }
}

export default TodoForm